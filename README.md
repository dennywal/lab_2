# README #

This was an astronomy lab in which I experimentally tested the binomial theorem.
The lab is written in an ipython (jupyter) notebook and requires the numpy, scipy and matplotlib libraries.